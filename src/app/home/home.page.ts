import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { Capacitor, Plugins } from '@capacitor/core';

import { LocationService } from '../location.service';
import { googlemaps } from 'googlemaps';
const { Geolocation, Toast } = Plugins;
declare var google: any;

// import { MapInfoWindow, MapMarker } from '@angular/google-maps';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  // @ViewChild(MapInfoWindow, { static: false }) infoWindow: MapInfoWindow;
  // lat = 22.2736308;
  // long = 70.7512555;

  // center = { lat: 24, lng: 12 };
  // markerOptions = { draggable: false };
  // markerPositions: google.maps.LatLngLiteral[] = [];
  // zoom = 4;
  // display?: google.maps.LatLngLiteral;

  // addMarker(event: google.maps.MouseEvent) {
  //   this.markerPositions.push(event.latLng.toJSON());
  // }

  // move(event: google.maps.MouseEvent) {
  //   this.display = event.latLng.toJSON();
  // }

  // openInfoWindow(marker: MapMarker) {
  //   this.infoWindow.open(marker);
  // }

  // removeLastMarker() {
  //   this.markerPositions.pop();
  // }
  lat: any;
  lng: any;
  watchId: any;
  constructor(public ngZone: NgZone, private locationService: LocationService) {
    this.lat = 12.93448;
    this.lng = 77.6192;
  }

  async getMyLocation() {
    const hasPermission = await this.locationService.checkGPSPermission();
    if (hasPermission) {
      if (Capacitor.isNative) {
        const canUseGPS = await this.locationService.askToTurnOnGPS();
        this.postGPSPermission(canUseGPS);
      } else {
        this.postGPSPermission(true);
      }
    } else {
      const permission = await this.locationService.requestGPSPermission();
      if (permission === 'CAN_REQUEST' || permission === 'GOT_PERMISSION') {
        if (Capacitor.isNative) {
          const canUseGPS = await this.locationService.askToTurnOnGPS();
          this.postGPSPermission(canUseGPS);
        } else {
          this.postGPSPermission(true);
        }
      } else {
        await Toast.show({
          text: 'User denied location permission',
        });
      }
    }
  }

  async postGPSPermission(canUseGPS: boolean) {
    if (canUseGPS) {
      this.watchPosition();
    } else {
      await Toast.show({
        text: 'Please turn on GPS to get location',
      });
    }
  }

  async watchPosition() {
    try {
      this.watchId = Geolocation.watchPosition({}, (position, err) => {
        this.ngZone.run(() => {
          if (err) {
            console.log('err', err);
            return;
          }
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          this.clearWatch();
        });
      });
    } catch (err) {
      console.log('err', err);
    }
  }

  clearWatch() {
    if (this.watchId != null) {
      Geolocation.clearWatch({ id: this.watchId });
    }
  }
}
